import React from 'react';
import {StyleSheet} from 'react-native';

import Animated, {
  withSpring,
  useAnimatedStyle,
  useAnimatedGestureHandler,
  runOnJS,
  useDerivedValue,
} from 'react-native-reanimated';

import {PanGestureHandler} from 'react-native-gesture-handler';
import {compare} from './helper';
import {correctSoundPlay} from './soundEffect';

import {useVector} from 'react-native-redash';

import {DAMPING} from './constants';

const SPRING_OPTIONS = {damping: DAMPING};

const AnimalDraggable = ({source, offsets, index}) => {
  const {original, target, isPanning, isValid, isTargetScale} = offsets[index];

  const pan = useVector();
  const panRelease = useVector();

  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      ctx.startX = pan.x.value;
      ctx.startY = pan.y.value;
      isPanning.value = true;
    },
    onActive: (event, ctx) => {
      pan.x.value = ctx.startX + event.translationX;
      pan.y.value = ctx.startY + event.translationY;

      panRelease.x.value = pan.x.value + original.x.value;
      panRelease.y.value = pan.y.value + original.y.value;

      if (compare(panRelease, target)) {
        isTargetScale.value = true;
      } else {
        isTargetScale.value = false;
      }
    },
    onEnd: (_, ctx) => {
      isPanning.value = false;
      console.log('pan', pan.x.value, pan.y.value);
      console.log(
        'x y ',
        pan.x.value + original.x.value,
        pan.y.value + original.y.value,
      );

      panRelease.x.value = pan.x.value + original.x.value;
      panRelease.y.value = pan.y.value + original.y.value;

      if (compare(panRelease, target)) {
        pan.x.value = target.x.value - original.x.value;
        pan.y.value = target.y.value - original.y.value;
        isValid.value = true;
        isTargetScale.value = false;

        runOnJS(correctSoundPlay)();
        // runOnJS(alert)();
      } else {
        runOnJS(correctSoundPlay)();
        pan.x.value = withSpring(0, SPRING_OPTIONS);
        pan.y.value = withSpring(0, SPRING_OPTIONS);
      }
    },
  });

  const translateX = useDerivedValue(() => {
    // if (isPanning.value) {
    //   return withSpring(pan.x.value, SPRING_OPTIONS);
    // }

    return withSpring(pan.x.value, SPRING_OPTIONS);
  });

  const translateY = useDerivedValue(() => {
    // if (isPanning.value) {
    //   return withSpring(pan.y.value, SPRING_OPTIONS);
    // }
    return withSpring(pan.y.value, SPRING_OPTIONS);
  });

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateX: translateX.value,
        },
        {
          translateY: translateY.value,
        },
      ],
    };
  });

  return (
    <PanGestureHandler onGestureEvent={gestureHandler}>
      <Animated.Image
        source={source}
        style={[styles.image, animatedStyle]}
        onLayout={({
          nativeEvent: {
            layout: {x, y},
          },
        }) => {
          console.log('layout ', x, y);
          original.x.value = x;
          original.y.value = y;
        }}
      />
    </PanGestureHandler>
  );
};

export default AnimalDraggable;

const styles = StyleSheet.create({
  image: {
    marginRight: 20,
  },
});
