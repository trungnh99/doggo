/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';

import Animated, {
  useSharedValue,
  withSpring,
  useAnimatedStyle,
  useAnimatedGestureHandler,
  runOnJS,
} from 'react-native-reanimated';
import {PanGestureHandler} from 'react-native-gesture-handler';
import {useVector} from 'react-native-redash';

import {Alert, StyleSheet, View, SafeAreaView} from 'react-native';
import RNBootSplash from 'react-native-bootsplash';

import {correctSoundPlay} from './soundEffect';
import {compare} from './helper';
import {DAMPING} from './constants';

const SPRING_OPTIONS = {DAMPING};

function App() {
  useEffect(() => {
    RNBootSplash.hide({fade: true});
  }, []);

  const pan = useVector(0);
  const original = useVector(0);
  const target = useVector(0);
  const panRelease = useVector(0);

  const isPanning = useSharedValue(false);
  const isValid = useSharedValue(false);

  function alert() {
    Alert.alert('Great move', '');
  }

  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      ctx.startX = pan.x.value;
      ctx.startY = pan.y.value;
      isPanning.value = true;
    },
    onActive: (event, ctx) => {
      pan.x.value = ctx.startX + event.translationX;
      pan.y.value = ctx.startY + event.translationY;

      panRelease.x.value = pan.x.value + original.x.value;
      panRelease.y.value = pan.y.value + original.y.value;

      if (compare(compare(panRelease, target))) {
        isValid.value = true;
      } else {
        isValid.value = false;
      }
    },
    onEnd: (_, ctx) => {
      isPanning.value = false;
      console.log('pan', pan.x.value, pan.y.value);
      console.log(
        'x y ',
        pan.x.value + original.x.value,
        pan.y.value + original.y.value,
      );

      panRelease.x.value = pan.x.value + original.x.value;
      panRelease.y.value = pan.y.value + original.y.value;

      if (compare(panRelease, target)) {
        pan.x.value = target.x.value - original.x.value;
        pan.y.value = target.y.value - original.y.value;
        isValid.value = false;

        runOnJS(correctSoundPlay)();
        runOnJS(alert)();
      } else {
        runOnJS(correctSoundPlay)();
        pan.x.value = withSpring(0, SPRING_OPTIONS);
        pan.y.value = withSpring(0, SPRING_OPTIONS);
      }
    },
  });

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateX: withSpring(pan.x.value, SPRING_OPTIONS),
        },
        {
          translateY: withSpring(pan.y.value, SPRING_OPTIONS),
        },
      ],
    };
  });

  const targetAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: withSpring(isValid.value ? 1.2 : 1),
        },
      ],
    };
  });

  return (
    <View
      style={{
        flex: 1,
      }}>
      <SafeAreaView />
      <PanGestureHandler onGestureEvent={gestureHandler}>
        <Animated.View
          style={[
            {
              borderWidth: 1,
            },
            styles.container,
          ]}>
          <Animated.View
            style={[
              styles.box,
              {
                backgroundColor: '#ccc',
              },
              targetAnimatedStyle,
            ]}
            onLayout={({nativeEvent: {layout}}) => {
              console.log('target layout ', layout);
              target.x.value = layout.x;
              target.y.value = layout.y;
            }}
          />

          <Animated.View
            style={[
              styles.box,
              animatedStyle,
              {
                position: 'absolute',
                bottom: 70,
                right: 20,
              },
            ]}
            onLayout={({nativeEvent: {layout}}) => {
              console.log('layout ', layout);
              original.x.value = layout.x;
              original.y.value = layout.y;
            }}
          />
        </Animated.View>
      </PanGestureHandler>
    </View>
  );
}

export default App;

const styles = StyleSheet.create({
  box: {
    height: 100,
    width: 100,
    backgroundColor: 'blue',
    borderRadius: 10,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
