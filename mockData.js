export const data = [
  {
    label: 'Cat',
    img: require('./assets/images/cat.png'),
  },
  {
    label: 'Fish',
    img: require('./assets/images/clown-fish.png'),
  },
  {
    label: 'Forest',
    img: require('./assets/images/forest.png'),
  },
  {
    label: 'Chicken',
    img: require('./assets/images/hen.png'),
  },
  //   {
  //     label: 'Dog',
  //     img: require('./assets/images/dog.png'),
  //   },
];
