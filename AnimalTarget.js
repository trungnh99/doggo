/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet} from 'react-native';

import Animated, {withSpring, useAnimatedStyle} from 'react-native-reanimated';
const AnimalTarget = ({source, offsets, index}) => {
  const {target, isTargetScale} = offsets[index];

  const targetAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: withSpring(isTargetScale.value ? 1.2 : 1),
        },
      ],
    };
  });

  return (
    <Animated.Image
      source={source}
      style={[styles.image, {tintColor: '#ccc'}, targetAnimatedStyle]}
      onLayout={({
        nativeEvent: {
          layout: {x, y},
        },
      }) => {
        console.log('target layout', x, y);
        target.x.value = x;
        target.y.value = y;
      }}
    />
  );
};

export default AnimalTarget;

const styles = StyleSheet.create({
  image: {
    marginRight: 20,
  },
});
