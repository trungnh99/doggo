import Sound from 'react-native-sound';
Sound.setCategory('Playback');

const correctSound = new Sound('correct.mp3', Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
});

export function correctSoundPlay() {
  correctSound.play();
}
