/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';

import {useSharedValue} from 'react-native-reanimated';
import {useVector} from 'react-native-redash';

import {StyleSheet, View} from 'react-native';
import RNBootSplash from 'react-native-bootsplash';

import {data} from './mockData';
import AnimalDraggable from './AnimalDraggable';
import AnimalTarget from './AnimalTarget';

function Animals() {
  useEffect(() => {
    RNBootSplash.hide({fade: true});
  }, []);

  const offsets = data.map(() => ({
    pan: useVector(0),
    original: useVector(0),
    target: useVector(0),
    panRelease: useVector(0),
    isValid: useSharedValue(false),
    isPanning: useSharedValue(false),
    isTargetScale: useSharedValue(false),
  }));

  return (
    <View style={styles.container}>
      <View
        style={[
          {
            flexDirection: 'row',
          },
        ]}>
        {data.map((el, idx) => (
          <AnimalTarget
            key={idx}
            index={idx}
            source={el.img}
            offsets={offsets}
          />
        ))}

        {data.map((el, idx) => (
          <AnimalDraggable
            key={idx}
            index={idx}
            source={el.img}
            offsets={offsets}
          />
        ))}
      </View>
    </View>
  );
}

export default Animals;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
