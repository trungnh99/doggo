export const compare = (_value, _target, _error = 40) => {
  'worklet';
  if (
    _value.x.value === _target.x.value &&
    _value.y.value === _target.y.value
  ) {
    return true;
  }

  if (
    Math.abs(_value.x.value - _target.x.value) < _error &&
    Math.abs(_value.y.value - _target.y.value) < _error
  ) {
    return true;
  }

  return false;
};
